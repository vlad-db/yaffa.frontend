var express = require('express'),
	app = express(),
	argv = require('yargs').argv,
	path = require('path'),
	port = argv.port || 5000

app.listen(port, function() {
	console.log(`Express server listening on port ${port}`)
})

app.use(express.static(path.join(__dirname,'../webapp/build')));