﻿var src = './webapp/src';
var dest = './webapp/build';
var path = require('path');

module.exports = {
    rootSrc: src,
    rootDest: dest,
    entries: [{
        name: 'main',
        js:  src + '/scripts/view/entries/main.jsx',
        style: src + '/styles/pages/main.less',
        template: src + '/pages/index.html'
    },{
        name: 'search',
        js:  src + '/scripts/view/entries/search.jsx',
        style: src + '/styles/pages/search.less',
        template: src + '/pages/index.html'
    },{
        name: 'place',
        js:  src + '/scripts/view/entries/place.jsx',
        style: src + '/styles/pages/place.less',
        template: src + '/pages/index.html'
    }],
    entriesDest: dest + '/pages',
    entriesPublic: '/pages/',
    jsStyle: {
        src: src + '/scripts/**/*.{js,jsx,json}'
    },
    img: {
        src: src + '/img/**',
        dest: dest + '/img'
    },
    svg: {
        src: src + '/svg/**/*.svg',
        dest: dest + '/img' 
    },
    fonts: {
        src: src + '/fonts/**',
        dest: dest + '/fonts'
    }
};