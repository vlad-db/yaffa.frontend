﻿var gulp = require('gulp');
var gulpif = require('gulp-if');
var bundleLogger = require('../util/bundleLogger');
var config = require('../config');
var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');
var gutil = require('gulp-util');

gulp.task('build', ['set-debug', 'set-watch', 'fonts', 'img', 'svg-sprites'], function(callback) {

    var entries = config.entries;
    var boundledCount = 0;
    function finishBundle(bundleName) {
        boundledCount++;
        if (boundledCount == entries.length) {
            callback();
            if(global.isWatching){
                gutil.log('Starting watch...');
            }
        }
    }
    entries.forEach(webpackThis);

    function webpackThis(entry) {
        var extractLess = new ExtractTextPlugin(`${entry.name}/${entry.name}.css`);
        var extractCss = new ExtractTextPlugin(`${entry.name}/${entry.name}.css`);
        function LoggerPlugin() {
            this.apply = function(compiler) {
                compiler.plugin('compile', function(err) {
                    bundleLogger.start(entry.name);
                });
                compiler.plugin('done', function(stats) {
                    var error;
                    if (stats.hasWarnings()) {
                        error = stats.compilation.warnings[0];
                    }
                    if (stats.hasErrors()) {
                        error = stats.compilation.errors[0];
                    }
                
                    if (error) {
                        try {
                            this.lastBuildSucceeded = false;
                            gutil.log(error.module.rawRequest + '\n' + error.error.toString());
                        } catch (e) {
                            gutil.log('Unknown error or warning');
                        }
                    }
                    if (!this.lastBuildSucceeded) {
                        this.lastBuildSucceeded = true;
                    }
                    bundleLogger.end(entry.name);
                });
            };
        }

        var webpackConfig = {
            cache: false,
            entry: ['babel-polyfill', entry.js, entry.style],
            output: {
                path: config.entriesDest,
                filename: `${entry.name}/${entry.name}.js`,
                publicPath: config.entriesPublic
            },
            watch: global.isWatching,
            devtool: global.debug ? 'source-map' : null,
            module: {
                loaders: [
                    {
                        test: /\.less$/i,
                        exclude: /node_modules/,
                        loader: extractLess.extract(['css?sourceMap', 'postcss', 'less?sourceMap'])
                    },
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['react', 'es2015'],
                            plugins: ['transform-decorators-legacy']
                        }
                    }
                ]
            },
            postcss() {
                return [autoprefixer];
            },
            plugins: [
                new webpack.DefinePlugin({
                    'process.env': {
                        'NODE_ENV': JSON.stringify(global.debug ? 'development' : 'production')
                    }
                }),
                extractLess,
                new HtmlWebpackPlugin({
                    template: entry.template,
                    filename: `${entry.name}/${entry.name}.html`
                }),
                new webpack.NoErrorsPlugin(),
                new LoggerPlugin()
            ]
        };

        //todo понять как сказать UglifyJsPlugin чтобы он вообще не трогал файлы, 
        //чтобы нормально сразу положить его в webpackConfig.plugins
        if (!global.debug) {
            webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin());
        }

        webpack(webpackConfig, onWebpackDone);
        function onWebpackDone(err, stats) {
            finishBundle(entry.name);
        }
    }
});