var gulp = require('gulp');
var argv = require('yargs').argv;

gulp.task('set-debug', function () {
    global.debug = argv.debug || false;
});