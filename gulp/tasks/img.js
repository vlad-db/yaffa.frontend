var gulp = require('gulp');
var changed = require('gulp-changed');
var img = require('../config').img;

gulp.task('img', function() {
    gulp
        .src(img.src)
        .pipe(changed(img.dest))
        .pipe(gulp.dest(img.dest))
});