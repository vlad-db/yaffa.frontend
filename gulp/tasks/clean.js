var gulp = require('gulp');
var clean = require('gulp-clean');
var rootBuild = require('../config').rootDest;

gulp.task('clean', function () {
	return gulp.src(rootBuild, {read: false})
		.pipe(clean({force: true}));
});