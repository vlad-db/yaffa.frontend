import { createApiAction } from '../../../src/scripts/redux/utils/async-actions.js';
import {assert, expect} from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares);

describe('Async action creator', function () {
     it('require 4 callback action', function () {

         assert.doesNotThrow(() => createApiAction({
             actionTypes: ['start', 'success', 'fail', 'done']
         }));
         
         assert.throw(() => createApiAction({
             actionTypes: ['start', 'success', 'fail']
         }));
     });
     it('return function', function () {
         var makeRequest = function () {
            var promise = new Promise();
             return promise;
         };
         
         var apiAction = createApiAction({
             makeRequest: makeRequest,
             actionTypes: ['start', 'success', 'fail', 'done']
         });
         var result = apiAction();
         expect(result).instanceOf(Function);
     });
     
     it('trigger actions from actionTypes param: [0](start) -> [1](success)/[2]fail -> [3]done ', function () {
         var promise = new Promise((resolve, reject) => resolve('response'));
         
         var makeRequest = function () {
             return promise;
         };
         
         var apiAction = createApiAction({
             makeRequest: makeRequest,
             actionTypes: ['start', 'success', 'fail', 'done']
         });
         
         var expectedActions = [
              { type: 'start', payload: 'request' },
              { type: 'success', payload: 'response' },
              { type: 'done', payload: 'request' },
         ];
         
         var store = mockStore();
         store.dispatch(apiAction('request'));
         expect(store.getActions()).to.equal(expectedActions);
     });
});