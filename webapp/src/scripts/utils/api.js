import * as http from './http';

import { assign } from 'lodash/object';

export const get = wrapHttp(http.get);

function wrapHttp(method) {
    return function(url, params = {}) {
        var apiUrl = 'http://185.58.204.135//api' + url;
        var apiParams = assign({
            headers: {
                'Accept': 'application/json',
                'Accept-Charset': 'utf-8',
                'Origin': 'www.test.ru',
                'Accept-Encoding': 'gzip, deflate, sdch'
            },
            redirect: 'follow',
            mode: 'cors'
        }, params);
        return method(apiUrl, apiParams);
    };
}
