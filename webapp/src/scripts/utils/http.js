require('whatwg-fetch');

import { assign } from 'lodash/object';

export function request(url, params = {}) {
    var options = assign({
        headers: {
            'Origin': 'www.test.ru'
        },
        redirect: 'follow',
        mode: 'cors'
    }, params);
    return fetch(url, options);
}

export function post(url, params = {}) {
    return request(url, assign({}, {
        method: 'POST',
        body: makeJsonBody(params)
    }, params));
}

export function get(url, params = {}) {
    return request(url, assign({}, {
        method: 'GET'
    }, params));
}

export function put(url, params = {}) {
    return request(url, assign( {}, {
        method: 'PUT',
        body: makeJsonBody(params)
    }, params));
}

export function remove(url, params = {}) {
    return request(url, assign({}, {
        method: 'DELETE',
        body: makeJsonBody(params)
    }, params));
}


function makeJsonBody(params) {
    if (!params) {
        return null;
    }
    return JSON.stringify(params);
}
