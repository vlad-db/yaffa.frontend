import { get } from '../utils/api';
import { reduce } from 'lodash/reduce';
import { buildQuery } from '../utils/url-query';


export function getPlaces(filter) {
    var params = {
        place_types: filter.placeTypes,
        features: filter.features,
        name: filter.searchText
    };
    return get('/places' + buildQuery(params, {encode: true}));
}

export function getPlace(placeId) {
    return get(`/places/${placeId}`);
}

export function getPlaceFeatures(placeId) {
    return get(`/places/${placeId}/features`);
}

export function getPlaceWorktimes(placeId) {
    return get(`/places/${placeId}/worktimes`);
}