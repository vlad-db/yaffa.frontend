import { each, reduce } from 'lodash/collection';
import { Iterable } from 'immutable';
import createLogger from 'redux-logger';

const loggerSettings = {
    stateTransformer: state => {
        return reduce(state, (memo, next, key) => {
            memo[key] = Iterable.isIterable(next) ? next.toJS() : next;
            return memo;
        }, {});
    }
};

export default createLogger(loggerSettings);