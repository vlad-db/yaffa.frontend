import logger from './logger';
import thunk from 'redux-thunk';
import combineActionsMiddleware from 'redux-combine-actions';
import { applyMiddleware } from 'redux';


export default applyMiddleware(
    thunk,
    combineActionsMiddleware,
    logger
);