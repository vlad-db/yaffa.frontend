import {combineReducers} from 'redux';
import * as searchActionTypes from './search-panel.action-types';
import featuresList from '../features/features.reducers';
import placeTypesList from '../place-types/place-types.reducers';

import {without} from 'lodash/array';

function toggleListItem(list = [], item) {
    var newList = [...list];
    if (newList.indexOf(item) < 0) {
        newList.push(item);
    } else {
        newList = without(newList, item);
    }
    return newList;
}

function search(state = {
    searchText: '',
    features: [],
    placeTypes: [],
    onlyNearby: false,
    openedNow: true,
    cardPayment: false
}, action) {
    switch (action.type) {
        case searchActionTypes.SET_SEARCH_TEXT:
            return Object.assign({}, state, {
                searchText: action.payload
            });
        case searchActionTypes.TOGGLE_FEATURE:
            return Object.assign({}, state, {
                features: toggleListItem(state.features, action.payload.id)
            });
        case searchActionTypes.TOGGLE_PLACE_TYPE:
            return Object.assign({}, state, {
                placeTypes: toggleListItem(state.placeTypes, action.payload.id)
            });
        case searchActionTypes.SET_WORKTIME_PARAM:
            return Object.assign({}, state, {
                openedNow: action.payload
            });
        case searchActionTypes.SET_LOCATION_PARAM:
            return Object.assign({}, state, {
                onlyNearby: action.payload
            });
        case searchActionTypes.SET_CARD_PAYMENT_PARAM:
            return Object.assign({}, state, {
                cardPayment: action.payload
            });
        default:
            return state;
    }
}

const searchPanel = combineReducers({
    search,
    featuresList,
    placeTypesList
});

export default  searchPanel;