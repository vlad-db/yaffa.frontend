import {createAction} from 'redux-actions';
import * as actionTypes from './search-panel.action-types';
import { Promise } from 'es6-promise';

const setSearchText = createAction(actionTypes.SET_SEARCH_TEXT);
const toggleFeature = createAction(actionTypes.TOGGLE_FEATURE);
const togglePlaceType = createAction(actionTypes.TOGGLE_PLACE_TYPE);
const setWorktimeSignificance = createAction(actionTypes.SET_WORKTIME_PARAM);
const setLocationSignificance = createAction(actionTypes.SET_LOCATION_PARAM);
const setCardPaymentSignificance = createAction(actionTypes.SET_CARD_PAYMENT_PARAM);

export {
    setSearchText,
    toggleFeature,
    togglePlaceType,
    setLocationSignificance,
    setWorktimeSignificance,
    setCardPaymentSignificance
};