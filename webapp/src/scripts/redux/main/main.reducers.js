import {combineReducers} from 'redux';
import searchPanel from '../search-panel/search-panel.reducers';


export default combineReducers({
    searchPanel
});