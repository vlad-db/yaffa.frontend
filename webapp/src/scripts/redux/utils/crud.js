import {handleActions} from 'redux-actions';
import {assign, merge} from 'lodash/object';
import {isString, isFunction} from 'lodash/lang';
import {identity} from 'lodash/util';

const defaultState = {
    pending: false
};

export function createUpdateReducer({ update, updateSuccess, updateFail, updateDone }) {
    return (state, action) => {
        if (!action.payload || state.id !== action.payload.id) {
            return state;
        }
        switch (action.type) {
            case update:
                return assign({}, state, { updatePending: true });
            case updateSuccess:
            case updateFail:
            case updateDone:
                return assign({}, state, { updatePending: false });
        }
    };
}

export function createReadReducer(dataPropertyName, [fetch, fetchSuccess, fetchFail, fetchDone], initialState) {
    let successActionType, successReducer = identity;

    if (isString(fetchSuccess)) {
        successActionType = fetchSuccess;
    } else {
        if (!fetchSuccess.type) {
            throw new Error('"fetchSuccess.type" is required');
        }
        if (!isFunction(fetchSuccess.handleAction)) {
            throw new Error('"fetchSuccess.handleAction" is not a function');
        }
        successActionType = fetchSuccess.type;
        successReducer = fetchSuccess.handleAction;
    }

    function fetchSuccessReducerWrapper(state, action) {
        let newState = assign({}, state, {
            [dataPropertyName]: action.payload
        });
        return successReducer(newState, action);
    }

    return (state = merge(initialState, defaultState), action) => {
        switch (action.type) {
            case fetch:
                return assign({}, state, { pending: true });
            case successActionType:
                return fetchSuccessReducerWrapper(state, action);
            case fetchFail:
            case fetchDone:
                return assign({}, state, { pending: false });
            default:
                return state;
        }
    };
}