import {createAction} from 'redux-actions';
import {httpService} from '../../utils/http';
import {identity} from 'lodash/util';
import {isFunction} from 'lodash/lang';
import {memoize} from  'lodash/function';
import { Schema, arrayOf, normalize } from 'normalizr';

// import { camelizeKeys } from 'humps';

const createOrGetAction = memoize(createAction);

export function createApiAction({
    makeRequest,
    metaCreator = null,
    successPayloadCreator = identity,
    mapResponseData = identity,
    actionTypes: [fetchStart, fetchSuccess, fetchFail, fetchDone],
    schema = null,
    normalizeOptions = null,
    successActionCreator = createAction,
    validator = null
}) {
    if (!fetchStart || !fetchSuccess || !fetchFail || !fetchDone) {
        throw new Error('fetch, fetchSuccess, fetchFail, fetchDone actionTypes are required');
    }

    return function () {
        const actionArgs = arguments;
        return (dispatch) => {
            let validationAction = tryDispatchFetchAction(dispatch, fetchStart, validator, actionArgs);
            if (validationAction && validationAction.err === 'validator') {
                dispatch(makeValidationFsa(validationAction, validator.failActionType, actionArgs));
                return;
            }
            const asyncAction = makeRequest(...actionArgs);
            return asyncAction
                .then(response => {
                    if (response instanceof Response) {
                        
                        if (!response.ok) {
                            response.json().then(error => {
                                error = new Error(error.Message);
                                dispatch(createOrGetAction(fetchFail)(error));
                            });
                            
                            return;
                        }
                        return response.json().then(processResponse);
                    } else {
                        return processResponse(response);
                    }
                    function processResponse(response) {
                        
                        const metaCreatorWrapper = metaCreator ? () => metaCreator(...actionArgs) : null;
                        const successPayloadCreatorWrapper = data => successPayloadCreator(data, ...actionArgs);
                        let createSuccessAction = successActionCreator(
                            fetchSuccess,
                            successPayloadCreatorWrapper,
                            metaCreatorWrapper);
                        let data = response;
                        if (data && data.data) {
                            data = data.data;
                        }
                        let responseData = mapResponseData(data, ...actionArgs);
                        if (responseData) {
                            // if(!dontCamelize) {
                            //     responseData = camelizeKeys(responseData);                                
                            // }
                            responseData = normalizeResponseData(responseData, schema, normalizeOptions);
                        } else if (schema) {
                            responseData = {
                                result: []
                            };
                        }
                        let successAction = createSuccessAction(responseData, ...actionArgs);
                        dispatch(successAction);
                        
                        return response;
                    }
                },
                    error => dispatch(createOrGetAction(fetchFail)()))
                .catch((response) => { })
                .then((response) => {
                    dispatch(createOrGetAction(fetchDone)(...actionArgs));
                });
        };
    };
}

function tryDispatchFetchAction(dispatch, actionType, validator, actionArgs) {
    if (validator) {
        if (!validator.failActionType) {
            throw new Error('validator.failActionType is required to dispatch validation error');
        }
        let fetchAction = createOrGetAction(actionType, identity, () => ({ validator: validator.rules }));
        return dispatch(fetchAction(...actionArgs));
    } else {
        let fetchAction = createOrGetAction(actionType);
        dispatch(fetchAction(...actionArgs));
    }
}

function makeValidationFsa(validationAction, actionType, actionArgs) {
    return {
        type: actionType,
        payload: {
            args: actionArgs,
            validationResult: validationAction
        }
    };
}

function normalizeResponseData(data, schema, options) {
    if (!schema) {
        return data;
    }
    if (!options) {
        return normalize(data, schema);
    }
    return normalize(data, schema, options);
}
