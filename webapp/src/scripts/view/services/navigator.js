import { buildQuery, parseQuery } from '../../utils/url-query';

const pages = {
    'main': {
        url: '/pages/main/main.html'
    },
    'search': {
        url: '/pages/search/search.html'
    },
    'place-details': {
        url: '/pages/place/place.html'
    }
};

export function getParams() {
    var params = parseQuery(window.location.search);
    return params;
}

export function getUrl(page, params = {}) {
    var url = pages[page].url;
    return url + buildQuery(params);
}

export function goTo(page, params = {}) {
    window.location.href = getUrl(page, params);
}