import React, {Component} from 'react';
import { getUrl } from '../services/navigator';
import SvgIcon from './common/svg-icon.jsx';

class Drawer extends Component {
    constructor() {
        super(...arguments);
        this.state = {
            expanded: false
        };
    }
    toggleDrawer() {
        this.setState({
            expanded: !this.state.expanded
        });
    }
    render() {
        return (
            <div>
                <SvgIcon icon="menu" className="layout-drawer__expander" onClick={() => this.toggleDrawer() } />
                <div className={'layout-drawer ' + (this.state.expanded ? 'layout-drawer--visible' : '') }>
                    <span className="layout-drawer__title">Меню</span>
                    <nav className="layout-drawer__navigation">
                        <a className="layout-drawer__navigation-link" href={getUrl('main')}>Главная</a>
                        <a className="layout-drawer__navigation-link" href="">События</a>
                        <a className="layout-drawer__navigation-link" href="">Акции и скидки</a>
                    </nav>
                </div>
                <div className={'layout-obfuscator ' + (this.state.expanded ? 'layout-obfuscator--visible' : '') }
                    onClick={() => this.toggleDrawer() }
                >
                </div>
            </div>
        );
    }
}

export default Drawer;