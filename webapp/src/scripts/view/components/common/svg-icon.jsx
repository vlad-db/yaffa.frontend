import React, {Component, PropTypes} from 'react';
import svg4everybody from 'svg4everybody';
import CombineClassName from '../../component-helpers/combine-classname';

@CombineClassName
class SvgIcon extends Component {
    constructor(...args) {
        super(...args);
    }
    getSvgIconPath(icon) {
        return `/img/svg-sprite.svg#${icon}`;
    }
    render() {
        var icon = this.getSvgIconPath(this.props.icon);
        return (
            <svg role="img" {...this.props}>
                <use xlinkHref={icon}/>
            </svg>
        );
    }
}

SvgIcon.propTypes = {
    icon: PropTypes.string.isRequired
};

svg4everybody();

export default SvgIcon;