import React, {Component} from 'react';
import { getUrl } from '../services/navigator';
import Drawer from './drawer.jsx';

class Header extends Component {
    render() {
        return (
            <header className="layout-header">
                <div className="layout-header__row">
                    <Drawer />
                    <span className="layout-header__title">YAFFA</span>
                    <nav className="layout-header__navigation">
                        <a className="layout-header__navigation-link" href={getUrl('main')}>Главная</a>
                        <a className="layout-header__navigation-link" href="">События</a>
                        <a className="layout-header__navigation-link" href="">Акции и скидки</a>
                    </nav>
                </div>
            </header>
        );
    }
}

export default Header;