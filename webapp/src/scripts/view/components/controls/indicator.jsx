import React, {Component, PropTypes} from 'react';
import { Switch } from './simple-controls.jsx';
import SvgIcon from '../common/svg-icon.jsx';

class Indicator extends Switch {
    render() {
        return (
            <label className={this.getClass()}>
                <input type="checkbox" id={this.switchId} ref="switch" onChange={this.props.onChange}/>
                <SvgIcon icon={this.props.icon} className="indicator__icon"/>
                <span className="indicator__label">{this.props.label}</span>
            </label>
        );
    }
    getInternalClass() {
        return 'indicator';
    }
}

Indicator.propTypes = {
    icon: PropTypes.string.isRequired
};

export default Indicator;