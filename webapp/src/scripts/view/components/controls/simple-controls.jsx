import React, {Component} from 'react';
import AbstractControl from './abstract-control.jsx';
import {isNil} from 'lodash/lang';
import {uniqueId} from 'lodash/util';

class Button extends AbstractControl {
    getInternalClass() {
        var classes = [
            'm-button',
            this.getTypeClass(),
            this.getColoredClass(),
            this.getStateClass()
        ];
        return classes.join(' ').trim();
    }
    getTypeClass() {
        var result = '';
        if (this.props.raised) {
            return 'm-button--raised';
        }
        if (this.props.fab) {
            return 'm-button--fab';
        }
        return '';
    }
    getColoredClass() {
        var result = '';
        if (this.props.primary) {
            return 'm-button--primary';
        }
        if (this.props.accent) {
            return 'm-button--accent';
        }
        return '';
    }
    getStateClass() {
        return '';
    }
    render() {
        return (
            <button className={this.getClass() } onClick={this.props.onClick}>{this.props.children}</button>
        );
    }
}


class Input extends AbstractControl {
    render() {
        return (
            <input type="text"
                ref="input"
                className={this.getClass() }
                onClick={this.props.onClick}
                onChange={this.props.onChange}
                defaultValue={this.props.value}
                placeholder={this.props.placeholder}
                />
        );
    }
    value(newValue) {
        if (isNil(newValue)) {
            return this.refs.input.value;
        } else {
            this.refs.input.value = newValue;
        }
    }
}

class Switch extends AbstractControl {
    constructor(props) {
        super(...arguments);
        this.switchId = uniqueId('switch');
    }
    getInternalClass() {
        return 'm-switch';
    }
    render() {
        return (
            <label className={this.getClass() } for={this.switchId}>
                <input type="checkbox" id={this.switchId} ref="switch" onChange={this.props.onChange}/>
                <span className="m-switch__thumb"></span>
                <span className="m-switch__label">
                    {this.props.label}
                </span>
            </label>
        );
    }
    value(newValue) {
        if (isNil(newValue)) {
            return this.refs.switch.checked;
        } else {
            this.refs.switch.checked = newValue;
        }
    }
}

class Select extends AbstractControl {
    getInternalClass() {
        return 'm-field';
    }
    render() {
        var options = this.props.options.map((item) => <option value="{item.value}" key={item.value}>{item.label}</option>);
        return (
            <div className={this.getClass() }>
                <select ref="select">
                    {options}
                </select>
                <div className="m-field__label">{this.props.label}</div>
                <i className="bar"></i>
            </div>
        );
    }
    value(newValue) {
        if (isNil(newValue)) {
            return this.refs.select.value;
        } else {
            this.refs.switch.value = value;
        }
    }
}

export {
Button,
Input,
Select,
Switch
};