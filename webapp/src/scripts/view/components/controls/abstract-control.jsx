import React, {Component} from 'react';
import CombineClassName from '../../component-helpers/combine-classname';

/**
 * Класс реализующий базовые методы всех контроллов
 */
@CombineClassName
class AbstractControl extends Component {
    componentDidMount() {
        if (super.componentDidMount) {
            super.componentDidMount(...arguments);
        }

        if (this.props.value) {
            this.value(this.props.value);
        }
    }
    getClass() {
        return this.combineClass(this.getInternalClass());
    }
    getInternalClass() {
        return '';
    }
    value() {
        console.error(`Method "value" are not implemented for ${this.constructor.name} controll`);
    }
}

export default AbstractControl;