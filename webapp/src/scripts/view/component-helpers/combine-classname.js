import mixin from './mixin-creator';

const CombineClassName = mixin({
  combineClass: function (className) {
      var result = '';
      if(this.props.className) {
          result += this.props.className;
      }
      if(className){
          result += className;
      }
      return result.trim();
  }
});

export default CombineClassName;