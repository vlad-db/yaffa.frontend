import React, {Component} from 'react';
import { Provider } from 'react-redux';
import ReactDom from 'react-dom';
import { createStore } from 'redux';
import searchReducer from '../../redux/search/search.reducers';
import docready from '../utils/docready';
import middlewares from '../../redux/middleware/middlewares';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import Drawer from '../components/drawer.jsx';
import { SearchPanelWithParams } from '../containers/search-panel/search-panel.jsx';
import PlacesList from '../containers/places-list/places-list.jsx';
import { getParams } from '../services/navigator';
import * as typify from '../services/typify';

var paramsSchema = typify.object({
    features: typify.arrayOf(typify.integer),
    placeTypes: typify.arrayOf(typify.integer),
    onlyNearby: typify.bool,
    openedNow: typify.bool,
    cardPayment: typify.bool
});
var params = typify.typify(getParams(), paramsSchema);

let store = createStore(searchReducer, { searchPanel: { search: params }, currentSearch: params }, middlewares);

docready(() => {
    ReactDom.render(
        <Provider store={store}>
            <div className="layout">
                <div className="layout__top-bg"></div>
                <Header />
                <div className="layout-content">
                    <SearchPanelWithParams />
                    <PlacesList />
                    <Footer />
                </div>
            </div>
        </Provider>,
        document.getElementById('domRoot')
    );
});
