import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ReactDom from 'react-dom';
import { createStore } from 'redux';
import mainReducer from '../../redux/main/main.reducers';
import docready from '../utils/docready';
import middlewares from '../../redux/middleware/middlewares';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import Drawer from '../components/drawer.jsx';
import { BaseSearchPanel } from '../containers/search-panel/search-panel.jsx';

let store = createStore(mainReducer, middlewares);

docready(() => {
    ReactDom.render(
        <Provider store={store}>
            <div>
                <div className="layout">
                    <div className="layout__top-bg"></div>
                    <Header />
                    <div className="layout-content">
                        <BaseSearchPanel />
                        <Footer />
                    </div>
                </div>
            </div>
        </Provider>,
        document.getElementById('domRoot')
    );
});
