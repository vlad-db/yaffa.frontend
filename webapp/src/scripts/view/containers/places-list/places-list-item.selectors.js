import { createSelector } from 'reselect';
import { filter } from 'lodash/collection';
import { concat } from 'lodash/array';
import { currentSearchParamsSelector } from '../search-panel/search-panel.selectors';

const placeSelector = (state, props) => props.place;
const featuresSelector = createSelector([placeSelector], place => place.found_features);
const typesSelector = createSelector([placeSelector], place => place.found_place_types);
const makeShowedFeaturesSelector = () => createSelector([featuresSelector, typesSelector], getPlaceFeatures);

function getPlaceFeatures(features, types) {
    return concat(features, types).map(item => {
        return {
            name: item.name,
            description: item.description
        };
    });
}

function placesListItemSelector() {
    return (state = {}, props = {}) => {
        return {
            search: currentSearchParamsSelector(state),
            showedTags: makeShowedFeaturesSelector()(state, props)
        };
    };
}

export default placesListItemSelector;