import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUrl } from '../../services/navigator';
import placesListSelector from './places-list-item.selectors';


class PlaceListItem extends Component {
    getDetailsUrl() {
        return getUrl('place-details', { placeId: this.props.place.id });
    }
    renderTags() {
        if(!this.props.showedTags) {
            return '';
        }
        
        function renderTagDescription(tag) {
            if(!tag.description) {
                return '';
            }
            return (
                <span className="place-card__feature-description">({tag.description})</span>
            );
        }
        return this.props.showedTags.map((tag) => (
            <div className="place-card__feature">
                <span className="place-card__feature-name">{tag.name}</span>
                {renderTagDescription(tag)}
            </div>
        ));
    }
    render() {
        return (
            <div className="place-card">
                <div className="place-card__image place-card__image--overlay place__image">
                    
                </div>
                <div className="place-card__content">
                    <a className="place-card__title" href={this.getDetailsUrl() }>
                        {this.props.place.name}
                    </a>
                    <div className="place-card__description">{this.props.place.address}</div>
                    {this.renderTags() }
                </div>
                
            </div>
        );
    }
}

PlaceListItem.propTypes = {
    place: PropTypes.object.isRequired,
    showedTags: PropTypes.array
};

function mapDispatch(dispatch) {
    return {
    };
}

export default connect(placesListSelector, mapDispatch)(PlaceListItem);


