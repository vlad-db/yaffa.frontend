import { createSelector } from 'reselect';
import { map } from 'lodash/collection';

export const searchPanelSlector = state => state.searchPanel;
export const searchParamsSelector = createSelector(searchPanelSlector, searchPanel => searchPanel.search);
export const searchTextSelector = createSelector([searchParamsSelector], search => search.searchText);
export const selectedFeaturesSelector = createSelector([searchParamsSelector], search => search.features);
export const selectedPlaceTypesSelector = createSelector([searchParamsSelector], search => search.placeTypes);
export const onlyNearbySelector = createSelector([searchParamsSelector], search => search.onlyNearby);
export const openedNowSelector = createSelector([searchParamsSelector], search => search.openedNow);
export const cardPaymentSelector = createSelector([searchParamsSelector], search => search.cardPayment);;
export const featuresListSelector = createSelector([searchPanelSlector], searchPanel => searchPanel.featuresList.features);
export const placeTypesListSelector = createSelector([searchPanelSlector], searchPanel => searchPanel.placeTypesList.placeTypes);
export const currentSearchParamsSelector = state => state.currentSearch;

const headerFeaturesSelector = createSelector(
    [currentSearchParamsSelector, featuresListSelector],
    (currentParams, list) => getEntityNames(currentParams.features, list)
);

const headerPlaceTypesSelector = createSelector(
    [currentSearchParamsSelector, placeTypesListSelector],
    (currentParams, list) => getEntityNames(currentParams.placeTypes, list)
);

function getEntityNames(ids, list) {
    let filtered = list.filter(item => ids.indexOf(item.id) > -1);
    return map(filtered, 'name');
}


export function searchPanelHeaderSelector(state = {}) {
    return {
        currentParams: currentSearchParamsSelector(state),
        featureNames: headerFeaturesSelector(state),
        placeTypeNames: headerPlaceTypesSelector(state)
        
    };
}

export function searchPanelDetailsSelector(state = {}) {
    return {
        search: searchParamsSelector(state),
        searchText: searchTextSelector(state),
        selectedFeatures: selectedFeaturesSelector(state),
        selectedPlaceTypes: selectedPlaceTypesSelector(state),
        featuresList: featuresListSelector(state),
        placeTypesList: placeTypesListSelector(state),
        onlyNearby: onlyNearbySelector(state),
        openedNow: openedNowSelector(state),
        cardPayment: cardPaymentSelector(state)
    };
}
