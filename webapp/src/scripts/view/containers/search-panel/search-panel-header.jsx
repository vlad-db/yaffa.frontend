import React, {Component} from 'react';
import { searchPanelHeaderSelector } from './search-panel.selectors';
import { connect} from 'react-redux';
import SvgIcon from '../../components/common/svg-icon.jsx';


export class SearchPanelTitleHeader extends Component {
    render() {
        return (
            <div className="search-panel__title">
                Куда пойдем?
            </div>
        );
    }
}

class SearchPanelParamsHeaderComponent extends Component {
    renderEnabledFlags() {
        let flags = [];
        function createIcon(icon) {
            return <SvgIcon className="header-params__flag" icon={icon}/>;
        }
        if(this.props.currentParams.onlyNearby) {
            flags.push(createIcon('walk'));
        }
        if(this.props.currentParams.openedNow) {
            flags.push(createIcon('timer'));
        }
        if(this.props.currentParams.cardPayment) {
            flags.push(createIcon('card'));
        }
        return flags;
    }
    render() {
        let tags = [...this.props.featureNames, ...this.props.placeTypeNames].join(', ');

        return (
            <div className="header-params">
                <div className="header-params__row">
                    <div className="header-params__search-text">
                        {this.props.currentParams.searchText}
                    </div>
                    <div className="header-params__search-flags">
                        {this.renderEnabledFlags()}
                    </div>
                </div>
                
                <div className="header-params__tags-list">
                    {tags}
                </div>
            </div>
        );
    }
}

export const SearchPanelParamsHeader = connect(searchPanelHeaderSelector)(SearchPanelParamsHeaderComponent);


