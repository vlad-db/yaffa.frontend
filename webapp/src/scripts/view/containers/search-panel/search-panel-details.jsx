import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect} from 'react-redux';
import { searchPanelDetailsSelector } from './search-panel.selectors';
import { fetchFeatures } from '../../../redux/features/features.actions';
import { fetchPlaceTypes } from '../../../redux/place-types/place-types.actions';
import * as searchPanelActions from '../../../redux/search-panel/search-panel.actions';
import { goTo } from '../../services/navigator';
import Indicator from '../../components/controls/indicator.jsx';
import { Input, Button, Switch, Select } from '../../components/controls/simple-controls.jsx';
import classNames from 'classnames';

class SearchPanelDetails extends Component {
    constructor() {
        super(...arguments);
        this.state = {
            swapTab: false
        };
    }
    componentDidMount() {
        this.props.fetchFeatures();
        this.props.fetchPlaceTypes();
    }
    getTabClass(forSwapped) {
        return classNames('tabs-block__item', {
            'tabs-block__item--selected': forSwapped == this.state.swapTab
        });
    }
    getTabContentClass() {
        return classNames('tabs-content-block__item', {
            'tabs-content-block__item--swapped': this.state.swapTab
        });
    }
    getTagClass(item, list) {
        return classNames('details-group__tag', {
            'details-group__tag--selected' : list && list.indexOf(item) > -1
        });
    }
    renderSelectedCounter(items) {
        let count = items.length;
        if(!count){
            return '';
        }
        return <span className="tabs-block__item-counter">{count}</span>;
    }
    renderTagsList(listAll, listSelected, idField, nameField, toggler, keyPrefix) {
        return listAll.map(item => {
            let idValue = item[idField];
            let nameValue = item[nameField];
            return (
                <span
                    className={this.getTagClass(idValue, listSelected)}
                    key={keyPrefix + nameValue}
                    onClick={() => toggler(item) }>
                    {nameValue}
                </span>
            );
        });
    }
    swapTab(swap) {
        this.setState({ swapTab: swap });
    }
    onSearchSubmit(e) {
        e.preventDefault();
        this.runSearch(this.props.search);
    }
    runSearch(searchParams) {
        goTo('search', searchParams);
    }
    render() {
        var input;
        var props = this.props;
        var features = this.renderTagsList(props.featuresList, props.selectedFeatures, 'id', 'name', props.toggleFeature, 'f');
        var placeTypes = this.renderTagsList(props.placeTypesList, props.selectedPlaceTypes, 'id', 'name', props.togglePlaceType, 't');
        return (
            <form onSubmit={(e) => this.onSearchSubmit(e) } className="search-details">
                <div className="indicators-block search-details__fixed-content">
                    <div className="indicators-block__item">
                        <Indicator icon="walk" label="Рядом" value={props.onlyNearby} ref="locationSwitch" onChange={() => props.setLocationSignificance(this.refs.locationSwitch.value()) }/>
                    </div>
                    <div className="indicators-block__item">
                        <Indicator icon="timer" label="Открыто" value={props.openedNow} ref="openedSwitch" onChange={() => props.setWorktimeSignificance(this.refs.openedSwitch.value()) }/>
                    </div>
                    <div className="indicators-block__item">
                        <Indicator icon="card" label="Оплата картой" value={props.cardPayment} ref="cardPaymennt" onChange={() => props.setCardPaymentSignificance(this.refs.cardPaymennt.value()) }/>
                    </div>
                </div>
                <div className="tabs-block search-details__fixed-content">
                    <div className={this.getTabClass(false) } onClick={() => this.swapTab(false) }>
                        Хочу...
                        {this.renderSelectedCounter(props.selectedFeatures)}
                    </div>
                    <div className={this.getTabClass(true) } onClick={() => this.swapTab(true) }>
                        Хочу в...
                        {this.renderSelectedCounter(props.selectedPlaceTypes)}
                    </div>
                    <div className='tabs-block__stub'>
                        
                    </div>
                </div>
                <div className="search-details__filters tabs-content-block">
                    <div className={this.getTabContentClass() }>
                        {features}
                    </div>
                    <div className={this.getTabContentClass() }>
                        {placeTypes}
                    </div>
                </div>
                <div className="search-details__fixed-content">
                    <div className="group-title">
                        Название и ключевые слова...
                    </div>
                    <Input className="search-details__input" value={props.searchText} ref={(node) => { input = node; } } onChange={() => props.setSearchText(input.value()) } placeholder="Введите название заведения"/>
                </div>
                <div className="search-details__actions search-details__fixed-content">
                    <button className="m-button m-button--flat search-details__run-search-btn"  type="submit" >Вперед</button>
                </div>
            </form>
        );
    }
}


function mapDispatchToProps(dispatch) {
    var actions = Object.assign({}, searchPanelActions, {
        fetchFeatures,
        fetchPlaceTypes
    });

    return bindActionCreators(actions, dispatch);
}

const searchPanelDetailsContainer = connect(searchPanelDetailsSelector, mapDispatchToProps)(SearchPanelDetails);

export default searchPanelDetailsContainer;