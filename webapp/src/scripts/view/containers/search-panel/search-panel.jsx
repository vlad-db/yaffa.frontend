import React, { Component } from 'react';
import SvgIcon from '../../components/common/svg-icon.jsx';
import ismobile from '../../utils/ismobile';
import { SearchPanelTitleHeader, SearchPanelParamsHeader } from './search-panel-header.jsx';
import SearchPanelDetails from './search-panel-details.jsx';
import classNames from 'classnames/dedupe'; 


export default class SearchPanel extends Component {
    constructor() {
        super(...arguments);
        this.state = {
            detailsExpanded: false,
            panelFixed: false,
            scrollEvents: ['scroll', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll', 'resize', 'touchmove']
        };
    }
    componentDidMount() {
        this.state.scrollEvents.forEach((type) => {
            window.addEventListener(type, this.handleScroll.bind(this), false);
        });
    }
    componentWillUnmount() {
        this.state.scrollEvents.forEach((type) => {
            window.removeEventListener(type, this.handleScroll.bind(this), false);
        });
    }
    handleScroll(e) {
        var panelRect = this.refs.panel.getBoundingClientRect();
        var shouldFixed = document.body.scrollTop >= 64;

        if (shouldFixed && !this.state.panelFixed) {
            this.setState({ panelFixed: true });
            return;
        }
        if (!shouldFixed && !!this.state.panelFixed) {
            this.setState({ panelFixed: false });
        }
    }
    toggleDetails() {
        document.body.className = classNames(document.body.className, { 'no-scroll': !this.state.detailsExpanded });
        this.setState({ detailsExpanded: !this.state.detailsExpanded });
    }
    getDetailsClass() {
        return classNames('search-panel__content', {
            'search-panel__content--expanded': this.state.detailsExpanded
        });
    }
    
    getDetailsHeight() {
        if (!ismobile() || !this.refs.panelHeader || !this.state.detailsExpanded) {
            return null;
        }
        var panelRect = this.refs.panelHeader.getBoundingClientRect();
        var resultHeight = window.screen.height - (panelRect.bottom - panelRect.top);
        return resultHeight;
    }
    
    getPanelClass() {
        return classNames('search-panel', {
            'search-panel--expanded': this.state.detailsExpanded,
            'search-panel--fixed': this.state.panelFixed 
        });
    }
    
    componentDidUpdate() {
        //todo: Если не решится вопрос, как нормально апдейтить инлайновые стили в IE,
        //то заменить этот ужас на нормальный json в аттрибут style
        if (!this.refs.panelContent) {
            return;
        }
        var height = this.getDetailsHeight();
        var state = this.state;
        function updateStyle(elem) {
            if (!state.detailsExpanded && elem.style.height) {
                elem.style.height = '';
            }

            if (state.detailsExpanded) {
                elem.style.height = height + 'px';

            }
        }
        updateStyle(this.refs.panelContent);
    }
    
    render() {
        console.log(this.props.header);
        return (
            <div className={this.getPanelClass()} ref="panel">
                <div className="search-panel__header " ref="panelHeader" onClick={() => this.toggleDetails() }>
                    {this.props.header}
                    <SvgIcon icon={this.state.detailsExpanded ? 'cross' : 'search'} className="search-details-expander"/>
                </div>
                <div className={this.getDetailsClass()} ref="panelContent">
                    <SearchPanelDetails/>
                </div>
            </div>
        );
    } 
}


export class BaseSearchPanel extends Component {
    render() {
        return (
            <SearchPanel header={<SearchPanelTitleHeader/>} />
        );
    }
}

export class SearchPanelWithParams extends Component {
    render() {
        return (
            <SearchPanel header={<SearchPanelParamsHeader/>} />
        );
    }
}
